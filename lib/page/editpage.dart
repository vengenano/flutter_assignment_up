import 'dart:convert';
import 'dart:io';

import 'package:final_assignement/actionController/producthandle.dart';
import 'package:final_assignement/model/productmodel.dart';
import 'package:final_assignement/page/mainpage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditPage extends StatefulWidget {
  Product product;
  EditPage({this.product});
  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  List<String> _accountType = <String>["Fruit", "Meat", "Vechetable"];
  String title = '';
  String price = '';
  String description = '';
  String selectedType = '';
  String imageUrlCtrl = '';
  String category = '';
  File _image;
var _titleCtrl = TextEditingController();

  Future getImage(bool isCamera) async {
    File image;
    if (isCamera == true) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);

      print(image);
    }
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Edit Product"),
      ),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 10.0),
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width / 2 - 100),
            child: Text(
              "Please Modify product",
              style: TextStyle(
                  color: Colors.orange,
                  fontSize: 25.0,
                  fontFamily: 'Oswald',
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30.0, right: 20.0),
            
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(labelText: 'Product title'),
                  onChanged: (String value) {
                    setState(() {
                      title = value;
                    });
                  },
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'product price'),
                  onChanged: (String value) {
                    setState(() {
                      price = value;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  maxLines: 3,
                  decoration: InputDecoration(labelText: 'Product Description'),
                  onChanged: (String value) {
                    setState(() {
                      description = value;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 14.0),
                      // child: Text(
                      //   "Choose", style: TextStyle(fontSize: 20.0),
                      // ),
                    ),
                    DropdownButton(
                      items: _accountType
                          .map((value) => DropdownMenuItem(
                                child: Text(
                                  value,
                                  style: TextStyle(fontSize: 17.0),
                                ),
                                value: value,
                              ))
                          .toList(),
                      onChanged: (String val) {
                        setState(() {
                          selectedType = val;
                        });
                      },
                      // value: selectedType,
                      hint: Text(
                          selectedType == 'Fruit'
                              ? "Fruit"
                              : selectedType == 'Meat'
                                  ? "Meat"
                                  : selectedType == 'Vechetable'
                                      ? "Vechetable"
                                      : "Choose Category type",
                          style: TextStyle(color: Colors.orange)),
                    ),
                  ],
                )),
                SizedBox(
                  height: 10.0,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      color: _image == null ? Colors.orange : Colors.white,
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: _image == null ? 200 : 140.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _image == null
                              ? Container(
                                  height: _image == null ? 190 : 140.0,
                                  margin: EdgeInsets.only(
                                      left: _image == null ? 5.0 : 0.0,
                                      right: _image == null ? 5.0 : 0.0),
                                  child: Image.asset(
                                      this.widget.product.imageUrl,fit: BoxFit.cover,),
                                )
                              : Container(
                                  height: 140.0,
                                  child: Image.file(
                                    _image,
                                    height: 300.0,
                                    width: 300.0,
                                  ),
                                )
                        ],
                      ),
                    ),
                    _image != null
                        ? Container()
                        : Positioned(
                            bottom: 10.0,
                            left: 15.0,
                            child: InkWell(
                              onTap: () {
                                getImage(true);
                              },
                              child: Icon(
                                Icons.camera_alt,
                                size: 35.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                    _image != null
                        ? Container()
                        : Positioned(
                            bottom: 7.0,
                            right: 15.0,
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: Colors.black,
                              child: Text('Browse file'),
                              onPressed: () {
                                getImage(false);
                              },
                            ),
                          ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RaisedButton(
                        textColor: Colors.white,
                        color: Colors.orangeAccent,
                        child: Text('Save'),
                        onPressed: () {
                          print(_image);
                          String imgpath = _image.toString();
                          Product product = Product(
                              productTitle: title,
                              productPrice: price,
                              productDesctiption: description,
                              // imageUrl: _image.toString(), get from camera
                              imageUrl:
                                  "https://www.lesoleilfruite.com/ressources/medias/93-hebergement-fruite-exterieur-620x330.jpg",
                              uploadate: "09/07/2019",
                              category: selectedType,
                              view: 1,
                              like: 1,
                              status: "true");
                          insertProduct(product);
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => MainPage()));
                        },
                      ),
                      RaisedButton(
                        textColor: Colors.white,
                        color: Colors.orangeAccent,
                        child: Text('Cancel'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
