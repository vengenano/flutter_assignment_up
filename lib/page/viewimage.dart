import 'package:flutter/material.dart';

class ViewImage extends StatelessWidget {
  var imageUrl;
  ViewImage({this.imageUrl});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.only(
                      top: 29.0, left: 5.0, right: 5.0, bottom: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    image: DecorationImage(
                        image: AssetImage(imageUrl), fit: BoxFit.cover),
                  ),
                ),
                Positioned(
                  top: 10.0,
                  left: 10.0,
                  child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.cancel,
                        color: Colors.red,
                        size: 30.0,
                      )),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
