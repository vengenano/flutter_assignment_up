import 'package:flutter/material.dart';

import 'mainpage.dart';

class Contact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: localeColor == 'black' ? Colors.black : Colors.orange,
        title: Text("Contact Us",style: TextStyle(fontSize: 22.0,fontWeight: FontWeight.bold,fontFamily: 'oswal'),),
      ),
      body: Container(
        width: double.infinity,
        color: localeColor=='black'?Colors.black:Colors.white,
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            Container(
              child: Text(
                "CONTACT US",
                style: TextStyle(
                    color: localeColor=='black'?Colors.white:Colors.black,
                    fontSize: 21.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'oswal'),
              ),
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0,bottom: 14.0),
                  child: Text(
                      "Keep your bases covered by including an “Other” or “General” category for those who may not find what they’re looking for in the list"
                      ,style:TextStyle(
                        
                        color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                ),
                Container(
                  child: Text("Company Information",style: TextStyle(color: Colors.orange,fontSize: 22.0,fontWeight: FontWeight.bold),),
                ),
                 Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text("Psa Touch",style:TextStyle(color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                      SizedBox(width: 25.0,),
                      Text("Freephone(PT) : +855 96 764 344 ",style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),)
                    ],
                  ),
                ),
                 Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text("Pas Tmey",style:TextStyle(color:localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                      SizedBox(width: 31.0,),
                      Text("FreePhone(PT) : +855 12 334 555",style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),)
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text("Suite 250",style:TextStyle(color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                      SizedBox(width: 35.0,),
                      Text("FreePhone(Khmer) : +855 23 23 23 23",style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),)
                    ],
                  ),
                ),Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text("Fix",style:TextStyle(color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                      SizedBox(width: 80.0,),
                      Text("Cambodia : +855 12 000 111",style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),)
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Row(
                    children: <Widget>[
                      Text(""),
                      SizedBox(width: 100.0,),
                      Text("Email : vengenorton@gmail.com",style:TextStyle(fontWeight: FontWeight.bold,fontSize: 15.0,color: localeColor=='black'?Colors.white:Colors.black))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.0,top: 10.0),
                  child: Column(
                    children: <Widget>[
                      Text("Office Hours",style:TextStyle(fontWeight: FontWeight.bold,color: localeColor=='black'?Colors.white:Colors.black,fontSize:19.0),),
                      Text("Monday - Friday 8AM-5PM",style:TextStyle(color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                      Text("It is Currently in Cambodia people",style:TextStyle(color: localeColor=='black'?Colors.white:Colors.black,fontWeight: FontWeight.bold,fontSize: 15.0)),
                    ],
                  ),
                )
                  

              ],
            )
          ],
        ),
      ),
    );
  }
}
