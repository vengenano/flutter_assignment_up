import 'dart:convert';
import 'dart:io';

import 'package:final_assignement/actionController/producthandle.dart';
import 'package:final_assignement/model/productmodel.dart';
import 'package:final_assignement/page/mainpage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  List<String> _accountType = <String>["Fruit", "Meat", "Vechetable"];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _title = '';
  String _price = '';
  String _description = '';
  String selectedType = '';
  String imageUrlCtrl = '';
  String category = '';
  bool _validateTitle = false;
  bool _validatePrice = false;
  bool _validateDescription = false;
  File _image;

  Future getImage(bool isCamera) async {
    File image;
    if (isCamera == true) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    }
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: localeColor == 'black' ? Colors.black : Colors.orange,
        title: Text("Add Product"),
      ),
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
        color: localeColor == 'black' ? Colors.black : Colors.orange,
        child: Form(
          key: _formKey,
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 2 - 100),
                      child: Text(
                        "Add Product Here",
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 25.0,
                            fontFamily: 'Oswald',
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30.0, right: 20.0),
                      child: Column(
                        children: <Widget>[
                          TextField(
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: localeColor == 'black'
                                      ? Colors.white
                                      : Colors.black),
                              labelText: 'Product title',
                              hintText: "Enter your email",
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.red)),
                              errorText: _validateTitle
                                  ? "Please fill the title is required"
                                  : null,
                            ),
                            // autofocus: true,
                            onChanged: (String value) {
                              print(value);
                              if (value.isEmpty) {
                                setState(() {
                                  _validateTitle = true;
                                });
                              } else {
                                setState(() {
                                  _validateTitle = false;
                                  _title = value;
                                });
                              }
                            },
                          ),
                          TextField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelStyle: TextStyle(
                                    color: localeColor == 'black'
                                        ? Colors.white
                                        : Colors.black),
                                labelText: 'product price',
                                errorText: _validatePrice
                                    ? "Please fill the price is required"
                                    : null),
                            onChanged: (String value) {
                              print(value);
                              if (value.isEmpty) {
                                setState(() {
                                  _validatePrice = true;
                                });
                              } else {
                                setState(() {
                                  _validatePrice = false;
                                  _price = value;
                                });
                              }
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          TextField(
                            maxLines: 3,
                            decoration: InputDecoration(
                                labelText: 'Product Description',
                                labelStyle: TextStyle(
                                    color: localeColor == 'black'
                                        ? Colors.white
                                        : Colors.black),
                                errorText: _validateDescription
                                    ? "Please fill the description is required"
                                    : null),
                            onChanged: (String value) {
                              print(value);
                              if (value.isEmpty) {
                                setState(() {
                                  _validateDescription = true;
                                });
                              } else {
                                setState(() {
                                  _validateDescription = false;
                                  _description = value;
                                });
                              }
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 14.0),
                                child: Text(
                                  "Type   ",
                                  style: TextStyle(fontSize: 16.0),
                                ),
                              ),
                              DropdownButton(
                                items: _accountType
                                    .map((value) => DropdownMenuItem(
                                          child: Text(
                                            value,
                                            style: TextStyle(fontSize: 17.0),
                                          ),
                                          value: value,
                                        ))
                                    .toList(),
                                onChanged: (String value) {
                                  setState(() {
                                    selectedType = value;
                                  });
                                },
                                // value: selectedType,
                                hint: Text(
                                    selectedType == 'Fruit'
                                        ? "Fruit"
                                        : selectedType == 'Meat'
                                            ? "Meat"
                                            : selectedType == 'Vechetable'
                                                ? "Vechetable"
                                                : "Select Category type",
                                    style: TextStyle(color: Colors.orange)),
                              ),
                            ],
                          )),
                          SizedBox(
                            height: 10.0,
                          ),
                          Stack(
                            children: <Widget>[
                              Container(
                                color: _image == null
                                    ? Colors.orange
                                    : Colors.white,
                                width: MediaQuery.of(context).size.width * 0.9,
                                height: _image == null ? 200 : 140.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    _image == null
                                        ? Container(
                                            height:
                                                _image == null ? 190 : 140.0,
                                            margin: EdgeInsets.only(
                                                left:
                                                    _image == null ? 5.0 : 0.0,
                                                right:
                                                    _image == null ? 5.0 : 0.0),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/defaultimg.png'),
                                                  fit: BoxFit.cover),
                                            ),
                                          )
                                        : Container(
                                            height: 140.0,
                                            child: Image.file(
                                              _image,
                                              height: 300.0,
                                              width: 300.0,
                                            ),
                                          )
                                  ],
                                ),
                              ),
                              _image != null
                                  ? Container()
                                  : Positioned(
                                      bottom: 10.0,
                                      left: 15.0,
                                      child: InkWell(
                                        onTap: () {
                                          getImage(true);
                                        },
                                        child: Icon(
                                          Icons.camera_alt,
                                          size: 35.0,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ),
                              _image != null
                                  ? Container()
                                  : Positioned(
                                      bottom: 7.0,
                                      right: 15.0,
                                      child: RaisedButton(
                                        textColor: Colors.white,
                                        color: Colors.black,
                                        child: Text('Browse file'),
                                        onPressed: () {
                                          getImage(false);
                                        },
                                      ),
                                    ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                RaisedButton(
                                  textColor: Colors.white,
                                  color: Colors.orangeAccent,
                                  child: Text('Save'),
                                  onPressed: () {
                                    String imgpath = _image.toString();
                                    {
                                      if (_title.isEmpty) {
                                        setState(() {
                                          _validateTitle = true;
                                        });
                                      }
                                      if (_price.isEmpty) {
                                        setState(() {
                                          _validatePrice = true;
                                        });
                                      }
                                      if (_description.isEmpty) {
                                        setState(() {
                                          _validateDescription = true;
                                        });
                                      } else {
                                        Product product = Product(
                                            productTitle: _title,
                                            productPrice: _price,
                                            productDesctiption: _description,
                                            imageUrl: _image
                                                .toString(), //get from camera
//                                        imageUrl:
//                                            "/storage/emulated/0/Android/data/com.example.final_assignement/files/Pictures/447085c2-4c2d-43be-b008-d1023043516f3144125371846553675.jpg",
                                            uploadate: "09/07/2019",
                                            category: selectedType,
                                            view: 0,
                                            like: 0,
                                            status: "true");
                                        insertProduct(product);
                                        Navigator.of(context).pushReplacement(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    MainPage()));
                                      }
                                    }
                                  },
                                ),
                                RaisedButton(
                                  textColor: Colors.white,
                                  color: Colors.orangeAccent,
                                  child: Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
        ));
  }
}
