import 'dart:io';
import 'package:final_assignement/actionController/producthandle.dart';
import 'package:final_assignement/model/productmodel.dart';
import 'package:final_assignement/page/about.dart';
import 'package:final_assignement/page/addproduct.dart';
import 'package:final_assignement/page/contact.dart';
import 'package:final_assignement/page/productdetail.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

String localeColor = 'white';

// import 'package:cached_network_image/cached_network_image.dart';
class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool alreadyLike = false;
  // String localeColor='white';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
      drawer: _buildDrawer(),
    );
  }

  _buildAppBar() {
    return AppBar(
        // backgroundColor: Colors.orange,
        backgroundColor: localeColor == 'black' ? Colors.black : Colors.orange,
        title: InkWell(
          onTap: () {},
          child: Row(
            children: <Widget>[
              Container(
                child: Container(
                  height: 50.0,
                  width: 190.0,
                  margin: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.white),
                  child: Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.search,
                          size: 20,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        Text(
                          "Search...",
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 30.0),
                child: Row(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                            child: InkWell(
                                onTap: () {
                                  print('Shoping card');
                                },
                                child: Icon(
                                  Icons.shopping_cart,
                                  size: 30.0,
                                  color: Colors.white,
                                ))),
                        Positioned(
                            top: 1.0,
                            left: 1.0,
                            child: Container(
                                width: 15.0,
                                height: 15.0,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(50.0)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '1',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13.0),
                                    ),
                                  ],
                                )))
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  _buildDrawer() {
    return Drawer(
      child: Container(
        // color: Colors.grey[200],
        color: localeColor == 'black' ? Colors.black : Colors.white,

        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      "assets/mslider.jpg",
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter,
                    ),
                    height: 100.0,
                    width: double.infinity,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  // Text("TOV PSAR"),
                  Text(
                    "TOV PSAR",
                    style: TextStyle(
                        color: localeColor == 'black'
                            ? Colors.white
                            : Colors.black),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                // color: Colors.black,
                color: localeColor == 'black' ? Colors.white : Colors.black,
                size: 28.0,
              ),
              title: Text(
                "Home",
                style: TextStyle(
                    color: localeColor == 'black' ? Colors.white : Colors.black,
                    fontSize: 18.0,
                    fontFamily: 'Oswald',
                    fontWeight: FontWeight.bold),
              ),
              trailing: Icon(Icons.arrow_forward_ios,
                  color: localeColor == 'black' ? Colors.white : Colors.black),
              onTap: () {
                print("home");
                Navigator.of(context).pop();
              },
            ),
            ListTile(
              leading: Icon(
                Icons.create,
                // color: Colors.black,
                color: localeColor == 'black' ? Colors.white : Colors.black,
                size: 28.0,
              ),
              title: Text("Add Produtct",
                  style: TextStyle(
                      color:
                          localeColor == 'black' ? Colors.white : Colors.black,
                      fontSize: 18.0,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.bold)),
              trailing: Icon(Icons.arrow_forward_ios,
                  color: localeColor == 'black' ? Colors.white : Colors.black),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => AddProduct()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.phone,
                color: Colors.blue,
                size: 28.0,
              ),
              title: Text("Contact us",
                  style: TextStyle(
                      color:
                          localeColor == 'black' ? Colors.white : Colors.black,
                      fontSize: 18.0,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.bold)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => Contact()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.help,
                color: Colors.orange,
                size: 28.0,
              ),
              title: Text("About me",
                  style: TextStyle(
                      color:
                          localeColor == 'black' ? Colors.white : Colors.black,
                      fontSize: 18.0,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.bold)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => About()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.power_settings_new,
                color: Colors.red,
                size: 28.0,
              ),
              title: Text("Change Theme",
                  style: TextStyle(
                      color:
                          localeColor == 'black' ? Colors.white : Colors.black,
                      fontSize: 18.0,
                      fontFamily: 'Oswald',
                      fontWeight: FontWeight.bold)),
              onTap: () {
                _showDialog();
              },
            ),
          ],
        ),
      ),
    );
  }

  _buildBody() {
    final screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight,
      child: Container(
        // color: Colors.white,
        color: localeColor == 'black' ? Colors.black : Colors.white,

        child: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection(COLLECTION_NAME).snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error.toString());
              if (snapshot.hasData) {
                List<Product> productList =
                    fromDocumentToproductList(snapshot.data.documents);
                return _buildListView(productList);
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }

  _buildListView(List<Product> productList) {
    return Container(
      child: ListView.builder(
          itemCount: productList.length,
          itemBuilder: (context, index) {
            return _buildListViewItem(productList[index]);
          }),
    );
  }

  _buildListViewItem(Product itemAtIndex) {
    final screenWidth = MediaQuery.of(context).size.width;
    var stringValue = itemAtIndex.imageUrl;
    var afterSubString = stringValue.substring(0, stringValue.length - 1);
    print(afterSubString.substring(7));
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(left: 10.0, top: 10.0),
                  child: Icon(Icons.play_arrow,
                      color: localeColor == 'black'
                          ? Colors.white
                          : Colors.black)),
              Container(
                  margin: EdgeInsets.only(top: 10.0, left: 6.0),
                  padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 3.0),
                  decoration: BoxDecoration(
                      // color: Colors.orange,
                      color:
                          localeColor == 'black' ? Colors.black : Colors.orange,
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Text('Fruit Gategory',
                      style: TextStyle(
                          color: localeColor == 'black'
                              ? Colors.white
                              : Colors.black,
                          fontSize: 18.0,
                          fontFamily: 'Oswald',
                          fontWeight: FontWeight.bold)))
            ],
          ),
          Container(
            width: screenWidth * 0.9,
            margin: EdgeInsets.only(top: 5.0),
            height: 270.0,
            child: Card(
              color: localeColor == 'black' ? Colors.black : Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      ),
                      child: InkWell(
                        onTap: () {
                          updateView(itemAtIndex, 1);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProductDetail(
                                    product: itemAtIndex,
                                  )));
                        },
                        // child: Image.network(
                        //   // '/storage/emulated/0/Android/data/com.example.final_assignement/files/Pictures/cc084e0b-b1ff-4e85-86c7-8c2463dff375574848837037117836.jpg',
                        //   itemAtIndex.imageUrl,
                        //   height: 200.0,
                        //   fit: BoxFit.cover,
                        // ),

//                          child: Image.asset(itemAtIndex.imageUrl.substring(7)),
//
                        child: Image.asset(afterSubString.substring(7),
                            height: 200.0, fit: BoxFit.cover),

//                           child: Image.asset('/storage/emulated/0/Android/data/com.example.final_assignement/files/Pictures/447085c2-4c2d-43be-b008-d1023043516f3144125371846553675.jpg'),

                        // child: CachedNetworkImage(
                        //   imageUrl: itemAtIndex.imageUrl,
                        //   height: 200.0,
                        //   placeholder: (context, url) => Center(
                        //         child: CircularProgressIndicator(),
                        //       ),
                        //   errorWidget: (context, url, error) => Container(
                        //         color: Colors.grey,
                        //       ),
                        // )
                      )),
                  Container(
                      padding: EdgeInsets.only(left: 10.0, top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Total:",
                                style: TextStyle(
                                    fontSize: 17.0,
                                    fontFamily: 'Oswald',
                                    color: localeColor == 'black'
                                        ? Colors.white
                                        : Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                " ${itemAtIndex.productPrice}",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: 'Oswald',
                                    color: Colors.orange,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "\$",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: 'Oswald',
                                    color: localeColor == 'black'
                                        ? Colors.orange
                                        : Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  updateLike(itemAtIndex, 1);
                                },
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.thumb_up,
                                      color: itemAtIndex.like <= 0
                                          ? Colors.grey
                                          : Colors.blue,
                                      size: 20.0,
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                    Text(
                                      itemAtIndex.like <= 0
                                          ? "Like"
                                          : itemAtIndex.like > 1
                                              ? "Likes: ${itemAtIndex.like}"
                                              : "Like: ${itemAtIndex.like}",
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontFamily: 'Oswald',
                                          color: itemAtIndex.like <= 0
                                              ? Colors.grey
                                              : Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.remove_red_eye,
                                color: itemAtIndex.view <= 0
                                    ? Colors.grey
                                    : Colors.blue,
                                size: 20.0,
                              ),
                              SizedBox(
                                width: 3.0,
                              ),
                              Text(
                                itemAtIndex.view <= 0
                                    ? "View"
                                    : itemAtIndex.view > 1
                                        ? "Views: ${itemAtIndex.view}"
                                        : "View: ${itemAtIndex.view}",
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: 'Oswald',
                                    color: itemAtIndex.view <= 0
                                        ? Colors.grey
                                        : Colors.blue,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showDialog() {
    String col = '';
    localeColor == 'black' ? col = 'white' : col = 'black';
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.black,
          title: new Text(
            "Change Them Comfirmation!",
            style: TextStyle(
                fontSize: 21.0,
                fontFamily: 'Oswald',
                color: Colors.orange,
                fontWeight: FontWeight.bold),
          ),
          content: new Text("Do you want to change them to ${col}?",
              style: TextStyle(
                  fontSize: 17.0,
                  fontFamily: 'Oswald',
                  // color: Colors.black,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
          actions: <Widget>[
            FlatButton(
              child: new Text("No",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: 'Oswald',
                      color: Colors.orange,
                      fontWeight: FontWeight.bold)),
              onPressed: () {
                getColorTheme();
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => MainPage()));
              },
            ),
            FlatButton(
              child: new Text("Yes",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: 'Oswald',
                      color: Colors.orange,
                      fontWeight: FontWeight.bold)),
              onPressed: () {
                localeColor=='black'?saveColorTheme('white'):saveColorTheme('black');
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => MainPage()));
              },
            ),
          ],
        );
      },
    );
  }

  saveColorTheme(String colorName) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('colorTheme', colorName);
    getColorTheme();
  }

  Future getColorTheme() async {
    final prefs = await SharedPreferences.getInstance();
    String value = prefs.getString('colorTheme');
    print(value);
    localeColor = value;
  }
}
