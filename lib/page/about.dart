import 'package:flutter/material.dart';

import 'mainpage.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: localeColor=='black'?Colors.black:Colors.white,
        title: Text(
          "About App",
          style: TextStyle(
              fontSize: 22.0, fontWeight: FontWeight.bold, fontFamily: 'oswal'),
        ),
      ),
      body: Container(
        width: double.infinity,
        color: localeColor=='black'?Colors.black:Colors.white,
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            Container(
              child: Text(
                "About Application",
                style: TextStyle(
                    color: localeColor=='black'?Colors.white:Colors.black,
                    fontSize: 28.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'oswal'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0),
              width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                children: <Widget>[
                  Text(
                    "TOV PSA is not a sample app that you know...",
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.bold,
                        fontFamily: "oswal",
                        fontSize: 22.0),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0,left: 10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "TOVPSA APP is began in 2005. After years in the App upload industry, we realized that it was near impossible for the average Jane or Joe to create their own website. Traditional web hosting services were simply too complicated, time consuming, and expensive to manage",
                    style: TextStyle(
                        color: localeColor=='black'?Colors.white:Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "oswal",
                        fontSize: 16.0),
                  )
                ],
              ),
            ),
            SizedBox(
              height:30.0,
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0,left: 10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "We created the Website.com Site Builder with the user's perspective in mind. We wanted to offer a platform that would require no coding skills or design experience. We keep it simple, so users can focus on creating an amazing website that reflects their brand. Best of all - it's free. You can get online, showcase your brand, or start selling products right away.",
                    style: TextStyle(
                        color: localeColor=='black'?Colors.white:Colors.black,
                        fontWeight: FontWeight.bold,
                        fontFamily: "oswal",
                        fontSize: 16.0),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
