import 'dart:io';

import 'package:final_assignement/actionController/producthandle.dart';
import 'package:final_assignement/model/productmodel.dart';
import 'package:final_assignement/page/mainpage.dart';
import 'package:final_assignement/page/viewimage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProductDetail extends StatefulWidget {
  Product product;
  ProductDetail({this.product});
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  bool _validateTitle = false;
  bool _validatePrice = false;
  bool _validateDescription = false;
  @override
  void initState() {
    super.initState();
    _titleCtrl.text = this.widget.product.productTitle;
    _priceCtrl.text = this.widget.product.productPrice;
    _descriptionCtrl.text = this.widget.product.productDesctiption;
    _categoryCtrl.text = this.widget.product.category;
    selectedType = this.widget.product.category;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.orange,
        backgroundColor: localeColor=='black'?Colors.black:Colors.orange,
        title: Text("Product Detail"),
      ),
      body: _buildBody(),
    );
  }

  _buildBody() {
    var stringValue = this.widget.product.imageUrl;
    var afterSubString = stringValue.substring(0, stringValue.length - 1);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        height: double.infinity,
        color:localeColor=='black'?Colors.black:Colors.white,
        child: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.all(10.0),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Positioned(
                        top: 130.0,
                        left: 150.0,
                        child: CircularProgressIndicator(),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ViewImage(
                                    imageUrl: afterSubString.substring(7),
                                  )));
                        },
                        child: Image.asset(
                          afterSubString.substring(7),
                          height: 300.0,
                          fit: BoxFit.cover,
                        ),
                      ),
//                      Container(
//                        child: Image.network(
//                          this.widget.product.imageUrl,
//                          height: 300.0,
//                          fit: BoxFit.cover,
//                        ),
//                      ),
                      Positioned(
                        left: 5.0,
                        child: InkWell(
                            onTap: () {
                              _showDialog();
                            },
                            child: Icon(Icons.clear,
                                color: Colors.red, size: 40.0)),
                      ),
                      Positioned(
                        right: 5.0,
                        child: InkWell(
                            onTap: () {
                              _showEditDialog();
                              //Navigator.of(context).push(MaterialPageRoute(
                              //builder: (context) => EditPage(product: this.widget.product,)));
                            },
                            child: Icon(Icons.edit,
                                color: Colors.orange, size: 40.0)),
                      ),
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 12.0, left: 20.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "Title :",
                                style: TextStyle(
                                    fontSize: 21.0,
                                    fontFamily: 'Oswald',
                                    // color: Colors.orange,
                                    color: localeColor=='black'?Colors.white:Colors.orange,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Text(
                                this.widget.product.productTitle.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontFamily: 'Oswald',
                                    // color: Colors.black,
                                    color: localeColor=='black'?Colors.white:Colors.orange,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                "Price :",
                                style: TextStyle(
                                    fontSize: 21.0,
                                    fontFamily: 'Oswald',
                                    // color: Colors.orange,
                                    color: localeColor=='black'?Colors.white:Colors.orange,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Text(
                                "${this.widget.product.productPrice}\$/1kg",
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontFamily: 'Oswald',
                                    // color: Colors.black,
                                    color: localeColor=='black'?Colors.white:Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: <Widget>[
                          //     Container(
                          //       margin: EdgeInsets.only(top: 8.0, bottom: 6.0),
                          //       child: Text(
                          //         "Description :",
                          //         style: TextStyle(
                          //             fontSize: 21.0,
                          //             fontFamily: 'Oswald',
                          //             color: Colors.orange,
                          //             fontWeight: FontWeight.bold),
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              this.widget.product.productDesctiption,
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontFamily: 'Oswald',
                                  // color: Colors.black,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  var _titleCtrl = TextEditingController();
  var _priceCtrl = TextEditingController();
  var _descriptionCtrl = TextEditingController();
  var _categoryCtrl = TextEditingController();
  String selectedType = '';
  bool isSelected = false;
  List<String> _accountType = <String>["Fruit", "Meat", "Vechetable"];

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: localeColor=='black'?Colors.black:Colors.white,
          title: new Text(
            "Delete Confirm!",
            style: TextStyle(
                fontSize: 21.0,
                fontFamily: 'Oswald',
                color: Colors.orange,
                fontWeight: FontWeight.bold),
          ),
          content: new Text("Do you want to delete this product?",
              style: TextStyle(
                  fontSize: 17.0,
                  fontFamily: 'Oswald',
                  // color: Colors.black,
                  color: localeColor=='black'?Colors.white:Colors.black,
                  fontWeight: FontWeight.bold)),
          actions: <Widget>[
            FlatButton(
              child: new Text("Close",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: 'Oswald',
                      color: Colors.orange,
                      fontWeight: FontWeight.bold)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Delete",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: 'Oswald',
                      color: Colors.orange,
                      fontWeight: FontWeight.bold)),
              onPressed: () {
                deleteProduct(this.widget.product);
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => MainPage()));
              },
            ),
          ],
        );
      },
    );
  }

  File _image;
  Future getImage(bool isCamera) async {
    File image;
    if (isCamera == true) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);

      print(image);
    }
    setState(() {
      _image = image;
    });
  }

  void _showEditDialog() {
    var stringValue = this.widget.product.imageUrl;
    var afterSubString = stringValue.substring(0, stringValue.length - 1);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: localeColor=='black'?Colors.black:Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          title: Stack(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.edit,
                    color: Colors.black,
                    size: 24.0,
                  ),
                  Text(
                    "Edit Product",
                    style: TextStyle(
                        fontSize: 19.0,
                        fontFamily: 'Oswald',
                        color: Colors.orange,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Positioned(
                right: -5.0,
                top: -5.0,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(
                    Icons.clear,
                    color: Colors.red,
                    size: 30.0,
                  ),
                ),
              )
            ],
          ),
          content: Container(
            height: 420.0,
            width: 300.0,
            child: ListView(
              children: <Widget>[
                TextField(
                  controller: _titleCtrl,
                  style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),
                  decoration: InputDecoration(
                    labelText: 'Product title',
                    labelStyle: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),
                    errorText: _validateTitle ? "title couldn't empty" : null,
                  ),
                  onChanged: (String value) {
                    if (value.isEmpty) {
                      setState(() {
                        _validateTitle = true;
                      });
                    } else {
                      setState(() {
                        _validateTitle = false;
                      });
                    }
                  },
                ),
                TextField(
                  controller: _priceCtrl,
                  style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),
                  decoration: InputDecoration(
                      labelText: 'Product Price',
                      labelStyle: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),
                      errorText:
                          _validatePrice ? "Price couldn't empty" : null),
                  onChanged: (String value) {
                    if (value.isEmpty) {
                      setState(() {
                        _validatePrice = true;
                      });
                    } else {
                      setState(() {
                        _validatePrice = false;
                      });
                    }
                  },
                ),
                Container(
                  child: TextField(
                    controller: _descriptionCtrl,
                    style: TextStyle(color: localeColor=='black'?Colors.white:Colors.black),
                    decoration:
                        InputDecoration(
                      labelStyle: TextStyle(color:localeColor=='black'?Colors.white:Colors.black),
                          labelText: 'Product Description'),
                    onChanged: (String value) {
                      print(value);
                    },
                  ),
                ),
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 14.0),
                      // child: Text(
                      //   "Choose", style: TextStyle(fontSize: 20.0),
                      // ),
                    ),
                    DropdownButton(
                      items: _accountType
                          .map((value) => DropdownMenuItem(
                                child: Text(
                                  value,
                                  style: TextStyle(fontSize: 17.0),
                                ),
                                value: value,
                              ))
                          .toList(),
                      onChanged: (String val) {
                        setState(() {
//                          selectedType = val;
                          isSelected = true;
                        });
                      },
                      // value: selectedType,
                      hint: Text(
                          selectedType == 'Fruit'
                              ? "Fruit"
                              : selectedType == 'Meat'
                                  ? "Meat"
                                  : selectedType == 'Vechetable'
                                      ? "Vechetable"
                                      : "Choose Category type",
                          style: TextStyle(color: Colors.orange)),
                    ),
                  ],
                )),
                Stack(
                  children: <Widget>[
                    Container(
                      color: _image == null ? Colors.orange : Colors.white,
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              getImage(true);
                            },
                            child: Image.asset(afterSubString.substring(7)),
//                              child: CachedNetworkImage(
//                                imageUrl: this.widget.product.imageUrl,
//                                placeholder: (context, url) => Center(
//                                      child: CircularProgressIndicator(),
//                                    ),
//                                errorWidget: (context, url, error) => Container(
//                                      color: Colors.grey,
//                                    ),
//                              ))

                            // Image.network(this.widget.product.imageUrl,
                            // fit: BoxFit.cover
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      bottom: 5.0,
                      left: 10.0,
                      child: InkWell(
                        onTap: () {
                          getImage(true);
                        },
                        child: Icon(
                          Icons.camera_alt,
                          size: 35.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 5.0,
                      right: 10.0,
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: Colors.black,
                        child: Text('Browse file'),
                        onPressed: () {
                          getImage(false);
                        },
                      ),
                    ),
                  ],
                ),
                // button edit
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        textColor: Colors.white,
                        color: Colors.orangeAccent,
                        child: Text('Edit'),
                        onPressed: () {
                          if (_titleCtrl.text.isEmpty) {
                            setState(() {
                              _validateTitle = true;
                            });
                          }
                          if (_priceCtrl.text.isEmpty) {
                            setState(() {
                              _validatePrice = true;
                            });
                          }
                          if (_descriptionCtrl.text.isEmpty) {
                            setState(() {
                              _validateDescription = true;
                            });
                          } else {
                            String imgpath = _image.toString();
                            updateProduct(this.widget.product, _titleCtrl.text,
                                _priceCtrl.text, _descriptionCtrl.text);
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => MainPage()));
                          }
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[],
        );
      },
    );
  }
}
