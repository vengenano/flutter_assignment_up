import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_assignement/model/productmodel.dart';

const String COLLECTION_NAME = "products";

insertProduct(Product product) {
  CollectionReference productRef =
      Firestore.instance.collection(COLLECTION_NAME);
  Firestore.instance.runTransaction((ts) async {
    await productRef.add(product.toJson()).whenComplete(() {
      print("Product was inserted");
    });
  });
}

updateAllProduct(Product product) {
  DocumentReference docRef = product.reference;
  print(docRef ?? "null docRef");
  Firestore.instance.runTransaction((ts) async {
    await docRef.updateData(product.toJson()).whenComplete(() {
      print("all product with id is updated.");
    });
  });
}

deleteProduct(Product product) {
  DocumentReference docRef = product.reference;
  print(docRef ?? "null docRef");
  Firestore.instance.runTransaction((ts) async {
    await docRef.delete().whenComplete(() {
      print("Product was deleted");
    });
  });

  Future<List<Product>> readproduct() async {
    CollectionReference productRef =
        Firestore.instance.collection("COLLECTION_NAME");
    QuerySnapshot querySnapshot = await productRef.getDocuments();
    List<DocumentSnapshot> documents = querySnapshot.documents;
    return fromDocumentToproductList(documents);
  }
}

fromDocumentToproductList(List<DocumentSnapshot> documents) {
  return documents.map((x) => Product.fromSnapshot(x)).toList();
}

updateLike(Product product, int num) {
  product.like += num;
  DocumentReference docRef = product.reference;
  Firestore.instance.runTransaction((ts) async {
    await docRef.updateData(product.toJson()).whenComplete(() {
      print("data updated.");
    });
  });
}

updateView(Product product, int num) {
  product.view += num;
  DocumentReference docRef = product.reference;
  Firestore.instance.runTransaction((ts) async {
    await docRef.updateData(product.toJson()).whenComplete(() {
      // print("data updated.");
    });
  });
}

updateProduct(Product product,String title,String price,String description) {
  product.productTitle=title;
  product.productPrice=price;
  product.productDesctiption=description;
  DocumentReference docRef = product.reference;
  Firestore.instance.runTransaction((ts) async {
    await docRef.updateData(product.toJson()).whenComplete(() {
      // print("data updated.");
    });
  });
}
