import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

Product productFromJson(String str) => Product.fromJson(json.decode(str));
String productToJson(Product data) => json.encode(data.toJson());

class Product {
  String productTitle;
  String productDesctiption;
  String imageUrl;
  String uploadate;
  String category;
  String productPrice;
  int view;
  int like;
  String status;
  DocumentReference reference;
  Product(
      {this.productTitle,
      this.productPrice,
      this.productDesctiption,
      this.imageUrl,
      this.uploadate,
      this.category,
      this.view,
      this.like,
      this.status});

  Product.fromJson(Map json, {this.reference}) {
    productTitle = json["productTitle"];
    productPrice = json["productPrice"];
    productDesctiption = json["productDesctiption"];
    imageUrl = json["imageUrl"];
    uploadate = json["uploadate"];
    category = json["category"];
    view = json["view"];
    like = json["like"];
    status = json["status"];
  }
  Product.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromJson(snapshot.data, reference: snapshot.reference);

  Map<String, dynamic> toJson() => {
        "productTitle": productTitle,
        "productPrice": productPrice,
        "productDesctiption": productDesctiption,
        "imageUrl": imageUrl,
        "uploadate": uploadate,
        "category": category,
        "view": view,
        "like": like,
        "status": status,
      };
}
